% Compute the error of a pose-pose constraint
% x1 3x1 vector (x,y,theta) of the first robot pose
% x2 3x1 vector (x,y,theta) of the second robot pose
% z 3x1 vector (x,y,theta) of the measurement
%
% You may use the functions v2t() and t2v() to compute
% a Homogeneous matrix out of a (x, y, theta) vector
% for computing the error.
%
% Output
% e 3x1 error of the constraint
% A 3x3 Jacobian wrt x1
% B 3x3 Jacobian wrt x2
function [e, A, B] = linearize_pose_pose_constraint(x1, x2, z)

  % TODO compute the error and the Jacobians of the error
    X1 = v2t(x1);
    X2 = v2t(x2);
    Z = v2t(z);
    e = t2v(Z\(X1\X2));
    
    R_i = X1(1:2,1:2);
    R_j = X2(1:2,1:2);
    R_ij = Z(1:2,1:2);
    
    th_i = x1(3);
    th_j = x2(3);
    th_ij = z(3);
   
    x_i = x1(1);
    y_i = x1(2);
    x_j = x2(1);
    y_j = x2(2);

    A = [-cos(th_i)*cos(th_ij)+sin(th_i)*sin(th_ij) -sin(th_i)*cos(th_ij)-cos(th_i)*sin(th_ij) 0; cos(th_i)*sin(th_ij)+sin(th_i)*cos(th_ij) sin(th_i)*sin(th_ij)-cos(th_i)*cos(th_ij) 0; 0 0 -1];

    A(1:2,3) = [cos(th_ij)*(-sin(th_i)*(x_j-x_i)+cos(th_i)*(y_j-y_i))+sin(th_ij)*(-cos(th_i)*(x_j-x_i)-sin(th_i)*(y_j-y_i)); -sin(th_ij)*(-sin(th_i)*(x_j-x_i)+cos(th_i)*(y_j-y_i))+cos(th_ij)*(-cos(th_i)*(x_j-x_i)-sin(th_i)*(y_j-y_i))];

    B = [cos(th_i)*cos(th_ij)-sin(th_i)*sin(th_ij) sin(th_i)*cos(th_ij)+cos(th_i)*sin(th_ij) 0; -cos(th_i)*sin(th_ij)-sin(th_i)*cos(th_ij) -sin(th_i)*sin(th_ij)+cos(th_i)*cos(th_ij) 0; 0 0 1];

end;