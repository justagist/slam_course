% Compute the error of a pose-landmark constraint
% x 3x1 vector (x,y,theta) of the robot pose
% l 2x1 vector (x,y) of the landmark
% z 2x1 vector (x,y) of the measurement, the position of the landmark in
%   the coordinate frame of the robot given by the vector x
%
% Output
% e 2x1 error of the constraint
% A 2x3 Jacobian wrt x
% B 2x2 Jacobian wrt l
function [e, A, B] = linearize_pose_landmark_constraint(x, l, z)

  % TODO compute the error and the Jacobians of the error
  X = v2t(x);
    
    R_i = X(1:2,1:2);

    e = R_i'*(l-x(1:2))-z;

    th_i = atan2(R_i(2,1),R_i(1,1));

    x_l = l(1); y_l = l(2);
    x_i = x(1); y_i = x(2);

    A = [-cos(th_i) -sin(th_i) -sin(th_i)*(x_l-x_i) + cos(th_i)*(y_l-y_i); sin(th_i) -cos(th_i) -cos(th_i)*(x_l-x_i)-sin(th_i)*(y_l-y_i)];
    B = [cos(th_i) sin(th_i); -sin(th_i) cos(th_i)];

end;
